# HMD Simulator
The HMD Simulator is an Unreal plugin that emulates a connected HMD and its controllers. Once the plugin is added to your project and enabled, you can [start in VR](Resources/VRLaunch.png) even if you do not have an HMD connected.

Please note, that this cannot replace real testing on an HMD, but allows simple tests without the appropriate hardware at hand.

[[_TOC_]]

## Adding to your project
- Ideally you should add this plugin as a submodule to your project by executing `git submodule add https://devhub.vr.rwth-aachen.de/VR-Group/unreal-development/tools/hmd-simulator.git Plugins/HMDSimulator` in the root of your project.
- After starting the editor you should check in `Edit->Plugins` if the plugin is enabled.
- As this plugin emulates an HMD like any other, you have to add the buttons of this HMD in your Action Mappings. For this go to `Edit->Project Settings->Engine->Input->Bindings->Action Mappings` and add the respective keys there. ![](Resources/InputBindings.png)

## Configuration
The HMD Simulator uses the keyboard and mouse to grab the input that is needed to emulate the controllers. As the simulator grabs these on a very high level, it catches key presses before your application can. Thus, the keys it uses can be reconfigured to fit your application. This can be done in the options of the Plugin. For this head to `Edit->Project Settings->Plugins->HMD Simulator` and configure everything to your liking. You can also change the displayed default controller model and some movement speeds there. Note that the controller model is only displayed if your project does not provide its own. ![](Resources/Options.png)

## Usage
{- Please note, that the keys mentioned in this section only refer to the default configuration -}

Do a [VR Preview launch](Resources/VRLaunch.png) and you will see something similar to this: ![](Resources/StandardView.png)
Currently, the HMD Simulator emulated the two controllers and the position of the HMD.

If SteamVR is running and a real HMD is detected it is used instead. If you want to explicitly use the HMDSimulator, switch off your HMD or unplug it and close SteamVR. Then restart Unreal und do a [VR Preview launch](Resources/VRLaunch.png).

### Camera Movement
You can move around with the Mouse + WASD

![](Resources/Movement.mp4)

### Controller Movement
You can select a controller to control with either LeftShift (Left Controller) or LeftAlt (Right Controller) or both at the same time. Once selected one or both controllers you can move them with the mouse on an orbit around you. You can also alter the radius of this orbit via the ScrollWheel. If you hold the Right-Mouse-Button you switch to rotation mode and the Mouse controls Pitch+Yaw of the controllers and the MouseWheel controls roll.

![](Resources/MoveAndRotate.mp4)

### Controller Buttons
The emulated controllers also offer 3 virtual buttons at the moment. The Trigger, Grab and Menu Button. These can be controlled with T, G or M respectively. If you select a controller (or more) and press one of these keys the controller sends a respective button press to Unreal, that you can use in your Blueprints (ideally via ActionMappings, see Configuration). If setup correctly you can e.g. Grab objects in the world and move them:

![](Resources/GrabRight.mp4)

### Packaging
The HMD Simulator can be packed together with your game and only turns on, if you launch your game in VR mode (commandline parameter `-vr`) and additionally provide the `-hmd-simulator` parameter.

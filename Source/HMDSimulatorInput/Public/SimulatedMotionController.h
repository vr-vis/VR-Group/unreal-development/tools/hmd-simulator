#pragma once

#include "CoreMinimal.h"
#include "IInputDevice.h"
#include "HMDSimulationManager.h"
#include "XRMotionControllerBase.h"

class HMDSIMULATORINPUT_API FSimulatedMotionController : public IInputDevice, public FXRMotionControllerBase {
public:
	
	/** Constructor that takes an initial message handler that will receive motion controller events */
	FSimulatedMotionController( const TSharedRef< FGenericApplicationMessageHandler >& InMessageHandler );

	/** Clean everything up */
	virtual ~FSimulatedMotionController();

	/* Keys */
	const FName HMDSimulatorKeyCategory = FName("HMD Simulator");
	const FKey HMDSimulator_LeftHand_Trigger = FKey("HMDSimulator_LeftHand_Trigger");
	const FKey HMDSimulator_LeftHand_Menu = FKey("HMDSimulator_LeftHand_Menu");
	const FKey HMDSimulator_LeftHand_Grip = FKey("HMDSimulator_LeftHand_Grip");
	const FKey HMDSimulator_RightHand_Trigger = FKey("HMDSimulator_RightHand_Trigger");
	const FKey HMDSimulator_RightHand_Menu = FKey("HMDSimulator_RightHand_Menu");
	const FKey HMDSimulator_RightHand_Grip = FKey("HMDSimulator_RightHand_Grip");

	void InitKeys();
	
private:

	// IInputDevice overrides
	virtual void Tick( float DeltaTime ) override;
	virtual void SendControllerEvents() override;
	virtual void SetMessageHandler( const TSharedRef< FGenericApplicationMessageHandler >& InMessageHandler ) override;
	virtual bool Exec( UWorld* InWorld, const TCHAR* Cmd, FOutputDevice& Ar ) override;
	virtual void SetChannelValue( int32 ControllerId, FForceFeedbackChannelType ChannelType, float Value ) override;
	virtual void SetChannelValues( int32 ControllerId, const FForceFeedbackValues& Values ) override;
	virtual bool SupportsForceFeedback(int32 ControllerId) override { return false; }

	// IMotionController overrides
	virtual void EnumerateSources(TArray<FMotionControllerSource>& SourcesOut) const override;
	virtual FName GetMotionControllerDeviceTypeName() const override;
	virtual bool GetControllerOrientationAndPosition( const int32 ControllerIndex, const EControllerHand DeviceHand, FRotator& OutOrientation, FVector& OutPosition, float WorldToMetersScale ) const override;
	virtual ETrackingStatus GetControllerTrackingStatus(const int32 ControllerIndex, const EControllerHand DeviceHand) const override;


	/** The recipient of motion controller input events */
	TSharedPtr< FGenericApplicationMessageHandler > MessageHandler;

private:
	UHMDSimulationManager* HMDSimulationManager = nullptr;

	void SendControllerEvent(bool& State, bool& StateDirty, const FKey& Key, int32 Id);

public:
	static const int32 LeftControllerDeviceId;
	static const int32 RightControllerDeviceId;
public:
	void SetHMDSimulationController(UHMDSimulationManager* Manager);

};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HMDSimulatorInputSettings.h"
#include "HMDSimulatorControllerModels.generated.h"

/**
 * This class only holds references to the models that are used by the MotionControllerAssetLibrary. By referencing them here, the models are cooked and packaged correctly.
 */
UCLASS()
class HMDSIMULATORINPUT_API UHMDSimulatorControllerModels : public UObject
{
	GENERATED_BODY()

	UHMDSimulatorControllerModels() {
		const ConstructorHelpers::FObjectFinder<UStaticMesh> ViveFinder = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/VREditor/Devices/Vive/VivePreControllerMesh.VivePreControllerMesh'"));
		if (ViveFinder.Succeeded()) ViveModel = ViveFinder.Object;

		const ConstructorHelpers::FObjectFinder<UStaticMesh> OculusFinder = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/VREditor/Devices/Oculus/OculusControllerMesh.OculusControllerMesh'"));
		if (OculusFinder.Succeeded()) OculusModel = OculusFinder.Object;

		CustomModel = GetDefault<UHMDSimulatorInputSettings>()->CustomControllerMesh;
	}

	UPROPERTY() UStaticMesh* ViveModel;
	UPROPERTY() UStaticMesh* OculusModel;
	UPROPERTY() UStaticMesh* CustomModel;
};

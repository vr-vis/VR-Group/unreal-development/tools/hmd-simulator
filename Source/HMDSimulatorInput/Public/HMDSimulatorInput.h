// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "IHeadMountedDisplayModule.h"
#include "Modules/ModuleManager.h"
#include "IInputDeviceModule.h"
#include "MotionControllerAssetManager.h"
#include "SimulatedMotionController.h"

class FHMDSimulatorInputModule : public IInputDeviceModule
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	/* IInputDeviceModule */
	TSharedPtr< class IInputDevice > CreateInputDevice(const TSharedRef< FGenericApplicationMessageHandler >& InMessageHandler) override;

private:
	TSharedPtr<FSimulatedMotionController> SimulatedMotionController;
	TSharedPtr<FMotionControllerAssetManager> AssetManager;
};

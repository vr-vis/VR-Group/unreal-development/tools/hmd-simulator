#pragma once

#include "CoreMinimal.h"
#include "IXRSystemAssets.h"

class HMDSIMULATORINPUT_API FMotionControllerAssetManager : public IXRSystemAssets {
public:
	const FName SystemName = TEXT("HMDSimulator");

	/** Clean everything up */
	virtual ~FMotionControllerAssetManager();

	virtual FName GetSystemName() const override;
	virtual bool EnumerateRenderableDevices(TArray<int32>& DeviceListOut) override;
	virtual int32 GetDeviceId(EControllerHand ControllerHand) override;
	virtual UPrimitiveComponent* CreateRenderComponent(const int32 DeviceId, AActor* Owner, EObjectFlags Flags, const bool bForceSynchronous = false, const FXRComponentLoadComplete& OnLoadComplete = FXRComponentLoadComplete()) override;
};

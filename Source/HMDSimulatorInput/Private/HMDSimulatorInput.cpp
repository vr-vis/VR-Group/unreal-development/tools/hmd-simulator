// Copyright Epic Games, Inc. All Rights Reserved.

#include "HMDSimulatorInput.h"

#include "IXRSystemAssets.h"
#include "IXRTrackingSystem.h"
#include "SimulatedMotionController.h"
#include "HMDSimulator.h"

#define LOCTEXT_NAMESPACE "FHMDSimulatorModule"

void FHMDSimulatorInputModule::StartupModule()
{
	IInputDeviceModule::StartupModule();

	AssetManager = MakeShared<FMotionControllerAssetManager>();
	IModularFeatures::Get().RegisterModularFeature(IXRSystemAssets::GetModularFeatureName(), AssetManager.Get());
}

void FHMDSimulatorInputModule::ShutdownModule()
{
	IInputDeviceModule::ShutdownModule();

	IModularFeatures::Get().UnregisterModularFeature(IXRSystemAssets::GetModularFeatureName(), AssetManager.Get());
	AssetManager.Reset();
}


TSharedPtr<IInputDevice> FHMDSimulatorInputModule::CreateInputDevice(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler)
{
	if (GEngine && GEngine->XRSystem.IsValid() && (GEngine->XRSystem->GetSystemName() == TEXT("HMDSimulator")))
	{
		SimulatedMotionController = MakeShared<FSimulatedMotionController>(InMessageHandler);
	    SimulatedMotionController->SetHMDSimulationController(FHMDSimulatorModule::Get().GetManager());
	    SimulatedMotionController->InitKeys();
	    return SimulatedMotionController;
	}
	return nullptr;
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FHMDSimulatorInputModule, HMDSimulatorInput)
#include "SimulatedMotionController.h"

const int32 FSimulatedMotionController::LeftControllerDeviceId = 1;
const int32 FSimulatedMotionController::RightControllerDeviceId = 2;

FSimulatedMotionController::FSimulatedMotionController(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler)
{
	MessageHandler = InMessageHandler;

	IModularFeatures::Get().RegisterModularFeature(GetModularFeatureName(), this);
}

FSimulatedMotionController::~FSimulatedMotionController()
{
}

void FSimulatedMotionController::InitKeys()
{
	EKeys::AddMenuCategoryDisplayInfo(HMDSimulatorKeyCategory,FText::AsCultureInvariant("HMD Simulator"), TEXT("GraphEditor.KeyEvent_16x"));
	
	EKeys::AddKey(FKeyDetails(HMDSimulator_LeftHand_Grip, FText::AsCultureInvariant("HMDSimulator (L) Grip Button"), FKeyDetails::GamepadKey, HMDSimulatorKeyCategory));
	EKeys::AddKey(FKeyDetails(HMDSimulator_LeftHand_Menu, FText::AsCultureInvariant("HMDSimulator (L) Menu Button"), FKeyDetails::GamepadKey, HMDSimulatorKeyCategory));
	EKeys::AddKey(FKeyDetails(HMDSimulator_LeftHand_Trigger, FText::AsCultureInvariant("HMDSimulator (L) Trigger Button"), FKeyDetails::GamepadKey, HMDSimulatorKeyCategory));

	EKeys::AddKey(FKeyDetails(HMDSimulator_RightHand_Grip, FText::AsCultureInvariant("HMDSimulator (R) Grip Button"), FKeyDetails::GamepadKey, HMDSimulatorKeyCategory));
	EKeys::AddKey(FKeyDetails(HMDSimulator_RightHand_Menu, FText::AsCultureInvariant("HMDSimulator (R) Menu Button"), FKeyDetails::GamepadKey, HMDSimulatorKeyCategory));
	EKeys::AddKey(FKeyDetails(HMDSimulator_RightHand_Trigger, FText::AsCultureInvariant("HMDSimulator (R) Trigger Button"), FKeyDetails::GamepadKey, HMDSimulatorKeyCategory));
}

void FSimulatedMotionController::Tick(float){}

void FSimulatedMotionController::SendControllerEvents()
{
	/* Triggers */
	SendControllerEvent(HMDSimulationManager->LeftHand.TriggerPressed, HMDSimulationManager->LeftHand.TriggerPressedDirty, HMDSimulator_LeftHand_Trigger, 0);
	SendControllerEvent(HMDSimulationManager->RightHand.TriggerPressed, HMDSimulationManager->RightHand.TriggerPressedDirty, HMDSimulator_RightHand_Trigger, 0);
	/* Menu */
	SendControllerEvent(HMDSimulationManager->LeftHand.MenuPressed, HMDSimulationManager->LeftHand.MenuPressedDirty, HMDSimulator_LeftHand_Menu, 0);
	SendControllerEvent(HMDSimulationManager->RightHand.MenuPressed, HMDSimulationManager->RightHand.MenuPressedDirty, HMDSimulator_RightHand_Menu, 0);
	/* Grip */
	SendControllerEvent(HMDSimulationManager->LeftHand.GripPressed, HMDSimulationManager->LeftHand.GripPressedDirty, HMDSimulator_LeftHand_Grip, 0);
	SendControllerEvent(HMDSimulationManager->RightHand.GripPressed, HMDSimulationManager->RightHand.GripPressedDirty, HMDSimulator_RightHand_Grip, 0);
}

void FSimulatedMotionController::SetMessageHandler(const TSharedRef<FGenericApplicationMessageHandler>& InMessageHandler)
{
	MessageHandler = InMessageHandler;
}

bool FSimulatedMotionController::Exec(UWorld* InWorld, const TCHAR* Cmd, FOutputDevice& Ar)
{
	return false;
}

void FSimulatedMotionController::SetChannelValue(int32, FForceFeedbackChannelType, float){}
void FSimulatedMotionController::SetChannelValues(int32, const FForceFeedbackValues&){}

void FSimulatedMotionController::EnumerateSources(TArray<FMotionControllerSource>& SourcesOut) const
{
	if(!HMDSimulationManager || !HMDSimulationManager->IsCurrentlyEnabled()) return;

	SourcesOut.Add(FMotionControllerSource(LeftHandSourceId));
	SourcesOut.Add(FMotionControllerSource(RightHandSourceId));
}

FName FSimulatedMotionController::GetMotionControllerDeviceTypeName() const
{
	const static FName DefaultName(TEXT("HMDSimulatorControllerDevice"));
	return DefaultName;
}

bool FSimulatedMotionController::GetControllerOrientationAndPosition(const int32, const EControllerHand DeviceHand, FRotator& OutOrientation, FVector& OutPosition, float) const
{
	if(!HMDSimulationManager || !HMDSimulationManager->IsCurrentlyEnabled()) return false;

	switch(DeviceHand)
	{
	case EControllerHand::Left:
		HMDSimulationManager->GetLeftHandPose(OutOrientation, OutPosition);
		return true;
	case EControllerHand::Right:
		HMDSimulationManager->GetRightHandPose(OutOrientation, OutPosition);
		return true;
	default: return false;
	}
}

ETrackingStatus FSimulatedMotionController::GetControllerTrackingStatus(const int32, const EControllerHand) const
{
	if(!HMDSimulationManager || !HMDSimulationManager->IsCurrentlyEnabled()) return ETrackingStatus::NotTracked;
	return ETrackingStatus::Tracked;
}

void FSimulatedMotionController::SendControllerEvent(bool& State, bool& StateDirty, const FKey& Key, int32 Id)
{
	if(StateDirty)
	{
		if(State)
		{
			MessageHandler->OnControllerButtonPressed(Key.GetFName(), Id, false);
		}
		else
		{
			MessageHandler->OnControllerButtonReleased(Key.GetFName(), Id, false);
		}
		StateDirty = false;
	}
}

void FSimulatedMotionController::SetHMDSimulationController(UHMDSimulationManager* Manager)
{
	HMDSimulationManager = Manager;
}

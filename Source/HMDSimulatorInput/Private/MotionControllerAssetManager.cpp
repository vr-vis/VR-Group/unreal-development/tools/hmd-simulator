#include "MotionControllerAssetManager.h"

#include "SimulatedMotionController.h"
#include "HMDSimulatorInputSettings.h"

FMotionControllerAssetManager::~FMotionControllerAssetManager()
{
}

FName FMotionControllerAssetManager::GetSystemName() const
{
	return SystemName;
}

bool FMotionControllerAssetManager::EnumerateRenderableDevices(TArray<int32>& DeviceListOut)
{
	DeviceListOut.AddUnique(FSimulatedMotionController::LeftControllerDeviceId);
	DeviceListOut.AddUnique(FSimulatedMotionController::RightControllerDeviceId);
	return true;
}

int32 FMotionControllerAssetManager::GetDeviceId(EControllerHand ControllerHand)
{
	switch(ControllerHand){
		case EControllerHand::Left: return FSimulatedMotionController::LeftControllerDeviceId;
		case EControllerHand::Right: return FSimulatedMotionController::RightControllerDeviceId;
		case EControllerHand::AnyHand: return FSimulatedMotionController::LeftControllerDeviceId;
		default: return INDEX_NONE;
	}
}

UPrimitiveComponent * FMotionControllerAssetManager::CreateRenderComponent(const int32 DeviceId, AActor * Owner, EObjectFlags Flags, const bool bForceSynchronous, const FXRComponentLoadComplete & OnLoadComplete)
{
	UPrimitiveComponent* NewRenderComponent = nullptr;

	UStaticMesh* ChosenMesh = nullptr;
	
	float YScale = 1.0f;
	switch(GetDefault<UHMDSimulatorInputSettings>()->DefaultDeviceModelForController)
	{
	case DCCM_Vive: {
		UStaticMesh* Vive = LoadObject<UStaticMesh>(Owner, TEXT("StaticMesh'/Engine/VREditor/Devices/Vive/VivePreControllerMesh.VivePreControllerMesh'")); 
		if(DeviceId == FSimulatedMotionController::LeftControllerDeviceId){
			ChosenMesh = Vive;
			YScale = 1;
		}
		if(DeviceId == FSimulatedMotionController::RightControllerDeviceId){
			ChosenMesh = Vive;
			YScale = -1;
		}
		break;
	}
	case DCCM_Oculus: {
		UStaticMesh* Oculus = LoadObject<UStaticMesh>(Owner, TEXT("StaticMesh'/Engine/VREditor/Devices/Oculus/OculusControllerMesh.OculusControllerMesh'"));
		if(DeviceId == FSimulatedMotionController::LeftControllerDeviceId){
			ChosenMesh = Oculus;
			YScale = 1;
		}
		if(DeviceId == FSimulatedMotionController::RightControllerDeviceId){
			ChosenMesh = Oculus;
			YScale = -1;
		}
		break;
	}
	case DCCM_Custom: {
		if(DeviceId == FSimulatedMotionController::LeftControllerDeviceId || DeviceId == FSimulatedMotionController::RightControllerDeviceId)
		{
			ChosenMesh = GetDefault<UHMDSimulatorInputSettings>()->CustomControllerMesh;
			YScale = 1;
		}
		break;
	}
	default: ChosenMesh = nullptr;
	}

	if (ChosenMesh)
	{
		const FName ComponentName = MakeUniqueObjectName(Owner, UStaticMeshComponent::StaticClass(), *FString::Printf(TEXT("%s_Device%d"), TEXT("HMDSimulator"), DeviceId));
		UStaticMeshComponent* MeshComponent = NewObject<UStaticMeshComponent>(Owner, ComponentName, Flags);

		MeshComponent->SetStaticMesh(ChosenMesh);
		MeshComponent->SetRelativeScale3D(MeshComponent->GetRelativeScale3D() * FVector(1, YScale, 1));
		NewRenderComponent = MeshComponent;
		
		NewRenderComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	OnLoadComplete.ExecuteIfBound(NewRenderComponent);
	return NewRenderComponent;
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "InputCoreTypes.h"
#include "Engine/DeveloperSettings.h"
#include "HMDSimulatorInputSettings.generated.h"

UENUM(BlueprintType)
enum EDefaultCustomControllerModel
{
	DCCM_Vive UMETA(DisplayName = "Vive"),
	DCCM_Oculus UMETA(DisplayName = "Oculus"),
	DCCM_Custom UMETA(DisplayName = "Custom")
};


/**
 * 
 */
UCLASS(config=Input, defaultconfig, meta=(DisplayName="HMD Simulator"))
class HMDSIMULATOR_API UHMDSimulatorInputSettings : public UDeveloperSettings
{
	GENERATED_BODY()

	virtual FName GetCategoryName() const override {return "Plugins";};

public:
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button for Forward Movement")) FKey Forward = EKeys::W;
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button for Backward Movement")) FKey Backward = EKeys::S;
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button for Left Movement")) FKey Left = EKeys::A;
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button for Right Movement")) FKey Right = EKeys::D;
																					  
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button for Trigger Action")) FKey Trigger = EKeys::T;
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button for Grip Action")) FKey Grip = EKeys::G;
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button for Menu Action")) FKey Menu = EKeys::M;
																					  
    UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button to Initiate Rotation")) FKey Rotation = EKeys::RightMouseButton;
																					  
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button to Select the Left Controller")) FKey SelectLeftController = EKeys::LeftShift;
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Button to Select the Right Controller")) FKey SelectRightController = EKeys::LeftAlt;

	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Axis for Horizontal Movement")) FKey HorizontalAxis = EKeys::MouseX;
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Axis for Vertical Movement")) FKey VerticalAxis = EKeys::MouseY;
	UPROPERTY(EditAnywhere, config, Category = "Key Mappings", meta = (DisplayName = "Axis for Distance Adjustment")) FKey DistanceAxis = EKeys::MouseWheelAxis;

	UPROPERTY(EditAnywhere, config, Category = "Speed | Camera", meta = (DisplayName = "Speed Multiplier for Horizontal Camera Rotation")) float LookAroundSpeedX = 1.0f;
	UPROPERTY(EditAnywhere, config, Category = "Speed | Camera", meta = (DisplayName = "Speed Multiplier for Vectical Camera Rotation")) float LookAroundSpeedY = 1.0f;

	UPROPERTY(EditAnywhere, config, Category = "Speed | Hands", meta = (DisplayName = "Speed Multiplier for Yaw Rotation of Hands")) float HandRotationYaw = 1.0f;
	UPROPERTY(EditAnywhere, config, Category = "Speed | Hands", meta = (DisplayName = "Speed Multiplier for Pitch Rotation of Hands")) float HandRotationPitch = 1.0f;
	UPROPERTY(EditAnywhere, config, Category = "Speed | Hands", meta = (DisplayName = "Speed Multiplier for Roll Rotation of Hands")) float HandRotationRoll = 2.0f;

	UPROPERTY(EditAnywhere, config, Category = "Speed | Camera", meta = (DisplayName = "Movement Speed (cm/s)")) float MovementSpeed = 100.0f;

	UPROPERTY(EditAnywhere, config, Category = "Contollers", meta = (DisplayName = "Default device model for controllers")) TEnumAsByte<EDefaultCustomControllerModel> DefaultDeviceModelForController = DCCM_Vive;
	UPROPERTY(EditAnywhere, Category = "Contollers", meta = (DisplayName = "Custom Default Mesh for Controller", EditCondition="DefaultDeviceModelForController==EDefaultCustomControllerModel::DCCM_Custom")) UStaticMesh* CustomControllerMesh = nullptr;
};

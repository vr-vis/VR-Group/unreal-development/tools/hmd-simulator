// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"
#include "HMDSimulationManager.h"



class HMDSIMULATOR_API FHMDSimulatorModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	UHMDSimulationManager* GetManager();

	static FHMDSimulatorModule& Get(){
		return FModuleManager::LoadModuleChecked<FHMDSimulatorModule>( "HMDSimulator" );
	}
	
private:
	TSharedPtr<UHMDSimulationManager> Manager;
};

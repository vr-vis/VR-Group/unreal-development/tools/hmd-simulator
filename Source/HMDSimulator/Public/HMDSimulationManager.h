#pragma once

#include "CoreMinimal.h"
#include "HeadMountedDisplayTypes.h"
#include "Framework/Application/IInputProcessor.h"

/* Hold Controller Data */
struct FControllerData
{
	FVector AccumulatedPosition = FVector::ZeroVector;
	FQuat AccumulatedRotation = FQuat::Identity;
	bool CurrentlyControlled = false;

	bool TriggerPressed = false;
	bool GripPressed = false;
	bool MenuPressed = false;

	/* Dirty Flags */
	bool TriggerPressedDirty = false;
	bool GripPressedDirty = false;
	bool MenuPressedDirty = false;

	void Reset(FVector Position = FVector::ZeroVector, FQuat Rotation = FQuat::Identity)
	{
		AccumulatedPosition = Position;
		AccumulatedRotation = Rotation;
		CurrentlyControlled = false;
		TriggerPressed = false;
		GripPressed = false;
		MenuPressed = false;
		TriggerPressedDirty = false;
		GripPressedDirty = false;
		MenuPressedDirty = false;
	}
};


class HMDSIMULATOR_API UHMDSimulationManager : public IInputProcessor {

public:
	UHMDSimulationManager();
	virtual ~UHMDSimulationManager();

	void GetHeadPose(FQuat& CurrentOrientation, FVector& CurrentPosition) const;
	void GetLeftHandPose(FRotator& CurrentOrientation, FVector& CurrentPosition) const;
	void GetRightHandPose(FRotator& CurrentOrientation, FVector& CurrentPosition) const;

    virtual void OnBeginPlay(FWorldContext& InWorldContext, bool EnableInThisPlay); //Called by simulated HMD
    virtual void OnEndPlay(FWorldContext& InWorldContext); //Called by simulated HMD

	bool IsCurrentlyEnabled(){return CurrentlyEnabled;}

private:
	void BindInputEvents();
	void UnbindInputEvents();
	UWorld* CurrentWorld = nullptr;
	bool CurrentlyEnabled = false;

public:
	FControllerData HMD;
	FControllerData LeftHand; /* Relative to HMD */
	FControllerData RightHand; /* Relative to HMD */
private:	
	bool RotationActivated = false;
	void InputRotation(bool Pressed);
	void InputSelectRightController(bool Pressed);
	void InputSelectLeftController(bool Pressed);
	
	void InputTrigger(bool Pressed);
	void InputGrip(bool Pressed);
	void InputMenu(bool Pressed);

	TMap<FKey, bool> MoveKeyPressed;
	void InputMoveRight(float Delta);
	void InputMoveForward(float Delta);

	void InputX(float Delta);
	void InputY(float Delta);
	void InputZ(float Delta);
	void InputXYZ(float DeltaX = 0, float DeltaY = 0, float DeltaZ = 0);

	/* IInputPreProcessor */
	TMap<FKey, TFunction<void ()>> KeyDownEventCallbacks;
	TMap<FKey, TFunction<void ()>> KeyUpEventCallbacks;
	TMap<FKey, TFunction<void(float)>> AnalogInputEventCallbacks;
	virtual bool HandleKeyDownEvent(FSlateApplication& SlateApp, const FKeyEvent& InKeyEvent) override;
	virtual bool HandleKeyUpEvent(FSlateApplication& SlateApp, const FKeyEvent& InKeyEvent) override;
	virtual bool HandleAnalogInputEvent(FSlateApplication& SlateApp, const FAnalogInputEvent& InAnalogInputEvent) override;
	virtual bool HandleMouseMoveEvent(FSlateApplication& SlateApp, const FPointerEvent& MouseEvent) override;
	virtual bool HandleMouseButtonDownEvent( FSlateApplication& SlateApp, const FPointerEvent& MouseEvent) override;
	virtual bool HandleMouseButtonUpEvent( FSlateApplication& SlateApp, const FPointerEvent& MouseEvent) override;
	virtual bool HandleMouseWheelOrGestureEvent(FSlateApplication& SlateApp, const FPointerEvent& InWheelEvent, const FPointerEvent* InGestureEvent) override;
	virtual void Tick(const float DeltaTime, FSlateApplication& SlateApp, TSharedRef<ICursor> Cursor) override;
	virtual const TCHAR* GetDebugName() const override { return TEXT("HMDSimulatorInputProcessor"); }
};

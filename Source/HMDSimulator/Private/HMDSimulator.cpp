// Copyright Epic Games, Inc. All Rights Reserved.

#include "HMDSimulator.h"

void FHMDSimulatorModule::StartupModule(){}

void FHMDSimulatorModule::ShutdownModule(){}

UHMDSimulationManager* FHMDSimulatorModule::GetManager()
{
	if(!Manager.IsValid())
	{
		Manager = MakeShared<UHMDSimulationManager>();
		FSlateApplication::Get().RegisterInputPreProcessor(Manager, 0);
		FSlateApplication::Get().OnPreShutdown().AddLambda([this](){
	        if(Manager.IsValid()){
		        FSlateApplication::Get().UnregisterInputPreProcessor(Manager);
		        Manager.Reset();
	        }
	    });
	}
	return Manager.Get();
}
	
IMPLEMENT_MODULE(FHMDSimulatorModule, HMDSimulator)
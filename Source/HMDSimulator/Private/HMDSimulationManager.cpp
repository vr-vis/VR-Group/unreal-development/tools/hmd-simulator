#include "HMDSimulationManager.h"

#include "HMDSimulatorInputSettings.h"
#include "Widgets/SViewport.h"

UHMDSimulationManager::UHMDSimulationManager()
{
	MoveKeyPressed.Add(EKeys::W, false);
	MoveKeyPressed.Add(EKeys::A, false);
	MoveKeyPressed.Add(EKeys::S, false);
	MoveKeyPressed.Add(EKeys::D, false);
}

void UHMDSimulationManager::OnBeginPlay(FWorldContext& InWorldContext, bool EnableInThisPlay)
{
	CurrentlyEnabled = EnableInThisPlay;
	CurrentWorld = InWorldContext.World();

	HMD.Reset();
	LeftHand.Reset(HMD.AccumulatedRotation.GetForwardVector()*50.0f + HMD.AccumulatedRotation.GetRightVector()*-25.0f);
	RightHand.Reset(HMD.AccumulatedRotation.GetForwardVector()*50.0f + HMD.AccumulatedRotation.GetRightVector()*25.0f);

	if(EnableInThisPlay)
	{
	    BindInputEvents();
	}
}

void UHMDSimulationManager::OnEndPlay(FWorldContext& InWorldContext)
{
	UnbindInputEvents(); //Do anyway
}

UHMDSimulationManager::~UHMDSimulationManager(){}

void UHMDSimulationManager::GetHeadPose(FQuat& CurrentOrientation, FVector& CurrentPosition) const
{
	CurrentOrientation = HMD.AccumulatedRotation;
	CurrentPosition = HMD.AccumulatedPosition;
}

void UHMDSimulationManager::GetLeftHandPose(FRotator& CurrentOrientation, FVector& CurrentPosition) const
{
	CurrentOrientation = FRotator(HMD.AccumulatedRotation * LeftHand.AccumulatedRotation);
	CurrentPosition = HMD.AccumulatedRotation.RotateVector(LeftHand.AccumulatedPosition) + HMD.AccumulatedPosition;
}

void UHMDSimulationManager::GetRightHandPose(FRotator& CurrentOrientation, FVector& CurrentPosition) const
{
	CurrentOrientation = FRotator(HMD.AccumulatedRotation * RightHand.AccumulatedRotation);
	CurrentPosition = HMD.AccumulatedRotation.RotateVector(RightHand.AccumulatedPosition) + HMD.AccumulatedPosition;
}

void UHMDSimulationManager::BindInputEvents()
{
	UnbindInputEvents();

	const UHMDSimulatorInputSettings* Settings = GetDefault<UHMDSimulatorInputSettings>();

	KeyDownEventCallbacks.Add(Settings->Rotation, [this](){InputRotation(true);});
	KeyUpEventCallbacks.Add(Settings->Rotation, [this](){InputRotation(false);});

	KeyDownEventCallbacks.Add(Settings->SelectLeftController, [this](){InputSelectLeftController(true);});
	KeyUpEventCallbacks.Add(Settings->SelectLeftController, [this](){InputSelectLeftController(false);});

	KeyDownEventCallbacks.Add(Settings->SelectRightController, [this](){InputSelectRightController(true);});
	KeyUpEventCallbacks.Add(Settings->SelectRightController, [this](){InputSelectRightController(false);});
	
	KeyDownEventCallbacks.Add(Settings->Trigger, [this](){InputTrigger(true);});
	KeyUpEventCallbacks.Add(Settings->Trigger, [this](){InputTrigger(false);});
	
	KeyDownEventCallbacks.Add(Settings->Menu, [this](){InputMenu(true);});
	KeyUpEventCallbacks.Add(Settings->Menu, [this](){InputMenu(false);});
	
	KeyDownEventCallbacks.Add(Settings->Grip, [this](){InputGrip(true);});
	KeyUpEventCallbacks.Add(Settings->Grip, [this](){InputGrip(false);});

	KeyDownEventCallbacks.Add(Settings->Forward, [this](){MoveKeyPressed[EKeys::W] = true;});
	KeyDownEventCallbacks.Add(Settings->Left, [this](){MoveKeyPressed[EKeys::A] = true;});
	KeyDownEventCallbacks.Add(Settings->Backward, [this](){MoveKeyPressed[EKeys::S] = true;});
	KeyDownEventCallbacks.Add(Settings->Right, [this](){MoveKeyPressed[EKeys::D] = true;});

	KeyUpEventCallbacks.Add(Settings->Forward, [this](){MoveKeyPressed[EKeys::W] = false;});
	KeyUpEventCallbacks.Add(Settings->Left, [this](){MoveKeyPressed[EKeys::A] = false;});
	KeyUpEventCallbacks.Add(Settings->Backward, [this](){MoveKeyPressed[EKeys::S] = false;});
	KeyUpEventCallbacks.Add(Settings->Right, [this](){MoveKeyPressed[EKeys::D] = false;});
	
	AnalogInputEventCallbacks.Add(Settings->HorizontalAxis, [this](float Value){InputX(Value);});
	AnalogInputEventCallbacks.Add(Settings->VerticalAxis, [this](float Value){InputY(Value);});
	AnalogInputEventCallbacks.Add(Settings->DistanceAxis, [this](float Value){InputZ(Value);});
}

void UHMDSimulationManager::UnbindInputEvents()
{
	KeyDownEventCallbacks.Reset();
	KeyUpEventCallbacks.Reset();
	AnalogInputEventCallbacks.Reset();
	RotationActivated = false;
}

void UHMDSimulationManager::InputMoveRight(float Delta)
{
	HMD.AccumulatedPosition += HMD.AccumulatedRotation.GetRightVector() * Delta;
}

void UHMDSimulationManager::InputMoveForward(float Delta)
{
	HMD.AccumulatedPosition += HMD.AccumulatedRotation.GetForwardVector() * Delta;
}

void UHMDSimulationManager::InputX(float Delta)
{
    if (Delta != 0.0f) InputXYZ(Delta, 0, 0); 
}

void UHMDSimulationManager::InputY(float Delta)
{
	if (Delta != 0.0f) InputXYZ(0, Delta, 0); 
}

void UHMDSimulationManager::InputZ(float Delta)
{
	if (Delta != 0.0f) InputXYZ(0, 0, Delta); 
}

void UHMDSimulationManager::InputXYZ(float DeltaX, float DeltaY, float DeltaZ)
{
	const UHMDSimulatorInputSettings* Settings = GetDefault<UHMDSimulatorInputSettings>();

	if (!LeftHand.CurrentlyControlled && !RightHand.CurrentlyControlled) /* Camera */
	{
		if (DeltaX != 0.0f){
			HMD.AccumulatedRotation = FQuat(FVector::UpVector, DeltaX * CurrentWorld->GetDeltaSeconds() * Settings->LookAroundSpeedX) * HMD.AccumulatedRotation;
		}
		if (DeltaY != 0.0f){
            FQuat NewRotation = FQuat(HMD.AccumulatedRotation.GetRightVector(), DeltaY * CurrentWorld->GetDeltaSeconds() * Settings->LookAroundSpeedY) * HMD.AccumulatedRotation;

			/* Clamp vertical rotation to +- 90� */
		    if(NewRotation.GetUpVector().Z < 0)
			{
			    FVector NewUp = NewRotation.GetUpVector();
				NewUp.Z = FMath::Max(0.0f, NewUp.Z);
				NewRotation = FRotationMatrix::MakeFromYZ(NewRotation.GetRightVector(), NewUp).ToQuat();
			}
				
			HMD.AccumulatedRotation = NewRotation;
		}
	}
	else
	{
		if (RotationActivated) /* Rotate */
		{
			const FQuat Rotation = FQuat::MakeFromEuler(FVector(DeltaZ, DeltaY, DeltaX) * FVector(Settings->HandRotationRoll, Settings->HandRotationPitch, Settings->HandRotationYaw));
			if(LeftHand.CurrentlyControlled && RightHand.CurrentlyControlled)
			{
				const FVector Midpoint = (LeftHand.AccumulatedPosition + RightHand.AccumulatedPosition) / 2;

				LeftHand.AccumulatedPosition  = Rotation.RotateVector(LeftHand.AccumulatedPosition - Midpoint) + Midpoint;
				RightHand.AccumulatedPosition = Rotation.RotateVector(RightHand.AccumulatedPosition - Midpoint) + Midpoint;
				LeftHand.AccumulatedRotation = Rotation * LeftHand.AccumulatedRotation;
				RightHand.AccumulatedRotation = Rotation * RightHand.AccumulatedRotation;
			}else{
				if(LeftHand.CurrentlyControlled)
				{
					LeftHand.AccumulatedRotation *= Rotation;
				}
				if(RightHand.CurrentlyControlled)
				{
					RightHand.AccumulatedRotation *= Rotation;
				}
			}
		}
		else /* Move */
		{
			if(LeftHand.CurrentlyControlled)   LeftHand.AccumulatedPosition += FVector(DeltaZ, DeltaX, DeltaY) * FVector(1.0f, 0.1f, -0.1f);
			if(RightHand.CurrentlyControlled) RightHand.AccumulatedPosition += FVector(DeltaZ, DeltaX, DeltaY) * FVector(1.0f, 0.1f, -0.1f);
		}
	}
}

void UHMDSimulationManager::InputSelectRightController(bool Pressed)
{
	RightHand.CurrentlyControlled = Pressed;
}

void UHMDSimulationManager::InputRotation(bool Pressed)
{
	RotationActivated = Pressed;
}
 
void UHMDSimulationManager::InputSelectLeftController(bool Pressed)
{
	LeftHand.CurrentlyControlled = Pressed;
}

void UHMDSimulationManager::InputTrigger(bool Pressed)
{
	if(LeftHand.CurrentlyControlled)
	{
		LeftHand.TriggerPressedDirty = LeftHand.TriggerPressed != Pressed;
		LeftHand.TriggerPressed = Pressed;
	}
	if(RightHand.CurrentlyControlled)
	{
		RightHand.TriggerPressedDirty = RightHand.TriggerPressed != Pressed;
		RightHand.TriggerPressed = Pressed;
	}
}

void UHMDSimulationManager::InputGrip(bool Pressed)
{
	if(LeftHand.CurrentlyControlled)
	{
		LeftHand.GripPressedDirty = LeftHand.GripPressed != Pressed;
		LeftHand.GripPressed = Pressed;
	}
	if(RightHand.CurrentlyControlled)
	{
		RightHand.GripPressedDirty = RightHand.GripPressed != Pressed;
		RightHand.GripPressed = Pressed;
	}
}

void UHMDSimulationManager::InputMenu(bool Pressed)
{
	if(LeftHand.CurrentlyControlled)
	{
		LeftHand.MenuPressedDirty = LeftHand.MenuPressed != Pressed;
		LeftHand.MenuPressed = Pressed;
	}
	if(RightHand.CurrentlyControlled)
	{
		RightHand.MenuPressedDirty = RightHand.MenuPressed != Pressed;
		RightHand.MenuPressed = Pressed;
	}
}

/*
 * 
 * IInputProcessor Functions
 * 
 */

bool CheckGameWindowFocus(FSlateApplication& SlateApp)
{
	FWidgetPath Path = FSlateApplication::Get().LocateWindowUnderMouse(FSlateApplication::Get().GetCursorPos(), FSlateApplication::Get().GetInteractiveTopLevelWindows(), true);
	if(!SlateApp.GetGameViewport().IsValid()) return false;

	return Path.ContainsWidget(SlateApp.GetGameViewport().ToSharedRef()) && SlateApp.HasUserMouseCapture(0);
}

bool UHMDSimulationManager::HandleKeyDownEvent(FSlateApplication& SlateApp, const FKeyEvent& InKeyEvent)
{
	if(!CheckGameWindowFocus(SlateApp)) return false;
	
	TFunction<void()>* FoundLambda = KeyDownEventCallbacks.Find(InKeyEvent.GetKey());
	if(FoundLambda)
	{
	    (*FoundLambda)();
		return true;
	}
	return false;
}

bool UHMDSimulationManager::HandleKeyUpEvent(FSlateApplication& SlateApp, const FKeyEvent& InKeyEvent)
{
	/* Explicitly ignore the focus for Shift-F1 to correctly process these events anyway */
	if(!CheckGameWindowFocus(SlateApp) && InKeyEvent.GetKey() != EKeys::LeftShift && InKeyEvent.GetKey() != EKeys::F1) return false;
	
	TFunction<void()>* FoundLambda = KeyUpEventCallbacks.Find(InKeyEvent.GetKey());
	if(FoundLambda)
	{
		(*FoundLambda)();
		return true;
	}
	return false;
}

bool UHMDSimulationManager::HandleAnalogInputEvent(FSlateApplication& SlateApp, const FAnalogInputEvent& InAnalogInputEvent)
{
	if(!CheckGameWindowFocus(SlateApp)) return false;
	
	TFunction<void(float)>* FoundLambda = AnalogInputEventCallbacks.Find(InAnalogInputEvent.GetKey());
	if(FoundLambda)
	{
		(*FoundLambda)(InAnalogInputEvent.GetAnalogValue());
		return true;
	}
	return false;
}

bool UHMDSimulationManager::HandleMouseMoveEvent(FSlateApplication& SlateApp, const FPointerEvent& MouseEvent)
{
	if(!CheckGameWindowFocus(SlateApp)) return false;

	bool Result = false;
	TFunction<void(float)>* FoundLambdaX = AnalogInputEventCallbacks.Find(EKeys::MouseX);
	TFunction<void(float)>* FoundLambdaY = AnalogInputEventCallbacks.Find(EKeys::MouseY);
	if(FoundLambdaX && MouseEvent.GetCursorDelta().X != 0)
	{
		(*FoundLambdaX)(MouseEvent.GetCursorDelta().X);
		Result = true;
	}
	if(FoundLambdaY && MouseEvent.GetCursorDelta().Y != 0)
	{
		(*FoundLambdaY)(MouseEvent.GetCursorDelta().Y);
		Result = true;
	}
	return Result;
}

bool UHMDSimulationManager::HandleMouseButtonDownEvent(FSlateApplication& SlateApp, const FPointerEvent& MouseEvent)
{
	if(!CheckGameWindowFocus(SlateApp)) return false;
	
	TFunction<void()>* FoundLambda = KeyDownEventCallbacks.Find(MouseEvent.GetEffectingButton());
	if(FoundLambda)
	{
		(*FoundLambda)();
		return true;
	}
	return false;
}

bool UHMDSimulationManager::HandleMouseButtonUpEvent(FSlateApplication& SlateApp, const FPointerEvent& MouseEvent)
{
	if(!CheckGameWindowFocus(SlateApp)) return false;
	
	TFunction<void()>* FoundLambda = KeyUpEventCallbacks.Find(MouseEvent.GetEffectingButton());
	if(FoundLambda)
	{
		(*FoundLambda)();
		return true;
	}
	return false;
}

bool UHMDSimulationManager::HandleMouseWheelOrGestureEvent(FSlateApplication& SlateApp, const FPointerEvent& InWheelEvent, const FPointerEvent* InGestureEvent)
{
	if(!CheckGameWindowFocus(SlateApp)) return false;
	
	TFunction<void(float)>* FoundLambda = AnalogInputEventCallbacks.Find(EKeys::MouseWheelAxis);
	if(FoundLambda)
	{
		(*FoundLambda)(InWheelEvent.GetWheelDelta());
		return true;
	}
	return false;
}

// The keys in this function actually are remapped on top. The enum is only used to transfer the intention to this function.
void UHMDSimulationManager::Tick(const float DeltaTime, FSlateApplication& SlateApp, TSharedRef<ICursor> Cursor)
{
	const float Speed = GetDefault<UHMDSimulatorInputSettings>()->MovementSpeed;

	if(MoveKeyPressed[EKeys::W])
	{
	    InputMoveForward(DeltaTime * Speed);
	}

    if(MoveKeyPressed[EKeys::S])
	{
	    InputMoveForward(-DeltaTime * Speed);
	}

	if(MoveKeyPressed[EKeys::D])
	{
	    InputMoveRight(DeltaTime * Speed);
	}

    if(MoveKeyPressed[EKeys::A])
	{
	    InputMoveRight(-DeltaTime * Speed);
	}
}

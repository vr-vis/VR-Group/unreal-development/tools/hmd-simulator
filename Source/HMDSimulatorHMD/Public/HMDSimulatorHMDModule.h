// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "IHeadMountedDisplayModule.h"
#include "Modules/ModuleManager.h"

class FHMDSimulatorHMDModule : public IHeadMountedDisplayModule
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	/** IHeadMountedDisplayModule implementation */
	virtual TSharedPtr<class IXRTrackingSystem, ESPMode::ThreadSafe> CreateTrackingSystem() override;
	
	FString GetModuleKeyName() const override { return FString(TEXT("HMDSimulatorHMD")); }

	virtual bool IsHMDConnected() override { return true; }
};

// Copyright Epic Games, Inc. All Rights Reserved.

#include "HMDSimulatorHMDModule.h"

#include "HMDSimulator.h"
#include "SimulatedHMD.h"

void FHMDSimulatorHMDModule::StartupModule()
{
	IHeadMountedDisplayModule::StartupModule();
}

void FHMDSimulatorHMDModule::ShutdownModule()
{
	IHeadMountedDisplayModule::ShutdownModule();
}

TSharedPtr<IXRTrackingSystem, ESPMode::ThreadSafe> FHMDSimulatorHMDModule::CreateTrackingSystem()
{
	TSharedPtr<FSimulatedHMD, ESPMode::ThreadSafe> HMD = FSceneViewExtensions::NewExtension<FSimulatedHMD>();
	HMD->SetHMDSimulationController(FHMDSimulatorModule::Get().GetManager());
	return HMD;
}
	
IMPLEMENT_MODULE(FHMDSimulatorHMDModule, HMDSimulatorHMD)
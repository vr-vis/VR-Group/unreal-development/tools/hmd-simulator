#include "SimulatedHMD.h"

#include "ClearQuad.h"
#include "CommonRenderResources.h"
#include "EngineGlobals.h"
#include "RenderUtils.h"
#include "ScreenRendering.h"

#include "Components/InputComponent.h"
#include "Engine/Engine.h"
#include "Engine/LocalPlayer.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/WorldSettings.h"
#include "Kismet/GameplayStatics.h"
#include "Misc/App.h"
#include "Modules/ModuleManager.h"
#include "GeneralProjectSettings.h"

#if WITH_EDITOR
#include "Settings/LevelEditorPlaySettings.h"
#endif

DEFINE_LOG_CATEGORY_STATIC(LogHMDSimulatiorHMD, Log, All);

const FName FSimulatedHMD::SystemName = TEXT("HMDSimulator");

float FSimulatedHMD::GetWorldToMetersScale() const
{
	return WorldToMeters;
}

EXRTrackedDeviceType FSimulatedHMD::GetTrackedDeviceType(int32 DeviceId) const
{
	if(DeviceId == HMDDeviceId) return EXRTrackedDeviceType::HeadMountedDisplay;
	return EXRTrackedDeviceType::Invalid;
}

//---------------------------------------------------
// SimpleHMD IHeadMountedDisplay Implementation
//---------------------------------------------------

bool FSimulatedHMD::IsHMDConnected() /* Always connected */
{
	return true;
}

bool FSimulatedHMD::IsHMDEnabled() const
{
	return HMDSimulationManager && HMDSimulationManager->IsCurrentlyEnabled();
}

void FSimulatedHMD::EnableHMD(bool){} /* Enabled/Disabled automatically (see CheckActivation) */

bool FSimulatedHMD::GetHMDMonitorInfo(MonitorInfo& MonitorDesc)
{
	MonitorDesc.MonitorName = "";
	MonitorDesc.MonitorId = 0;
	MonitorDesc.DesktopX = MonitorDesc.DesktopY = MonitorDesc.ResolutionX = MonitorDesc.ResolutionY = 0;
	return false;
}

void FSimulatedHMD::GetFieldOfView(float& OutHFOVInDegrees, float& OutVFOVInDegrees) const
{
	OutHFOVInDegrees = 0.0f;
	OutVFOVInDegrees = 0.0f;
}

bool FSimulatedHMD::CheckActivation()
{
#if WITH_EDITOR
	if (GIsEditor)
	{
		// Play in VR, but no other HMD available
		if(GetDefault<ULevelEditorPlaySettings>()->LastExecutedPlayModeType == PlayMode_InVR
			&& GEngine->XRSystem->CountTrackedDevices(EXRTrackedDeviceType::HeadMountedDisplay) <= 1)
		{
			return true;
	    }
	}
#endif

	// Launch via commandline and give `-vr -hmd-simulator`
	if(FParse::Param(FCommandLine::Get(), TEXT("hmd-simulator"))
		&& (FParse::Param(FCommandLine::Get(), TEXT("vr")) || GetDefault<UGeneralProjectSettings>()->bStartInVR))
	{
		return true;
	}

	return false;
}

void FSimulatedHMD::OnBeginPlay(FWorldContext& InWorldContext)
{
	const bool Activation = CheckActivation();

	UE_LOG(LogHMDSimulatiorHMD, Log, TEXT("HMDSimulator Activated: %d"), Activation);
	HMDSimulationManager->OnBeginPlay(InWorldContext, Activation);
}

void FSimulatedHMD::OnEndPlay(FWorldContext& InWorldContext)
{
    HMDSimulationManager->OnEndPlay(InWorldContext);
}

bool FSimulatedHMD::EnumerateTrackedDevices(TArray<int32>& OutDevices, EXRTrackedDeviceType Type)
{
	if (HMDSimulationManager && HMDSimulationManager->IsCurrentlyEnabled() &&
		(Type == EXRTrackedDeviceType::Any || Type == EXRTrackedDeviceType::HeadMountedDisplay))
	{
		OutDevices.Add(HMDDeviceId);
		return true;
	}
	
	return false;
}

void FSimulatedHMD::SetInterpupillaryDistance(float){}

float FSimulatedHMD::GetInterpupillaryDistance() const
{
	return 0.064f;
}

bool FSimulatedHMD::GetCurrentPose(int32 DeviceId, FQuat& CurrentOrientation, FVector& CurrentPosition)
{
	if(!HMDSimulationManager || !HMDSimulationManager->IsCurrentlyEnabled()) return false;

	if(DeviceId == HMDDeviceId)
	{
		HMDSimulationManager->GetHeadPose(CurrentOrientation, CurrentPosition);
		return true;
	}

	return false;
}

bool FSimulatedHMD::IsChromaAbCorrectionEnabled() const
{
	return false;
}

void FSimulatedHMD::ResetOrientationAndPosition(float Yaw)
{
	ResetOrientation(Yaw);
	ResetPosition();
}

void FSimulatedHMD::ResetOrientation(float Yaw)
{
	//TODO
}

void FSimulatedHMD::ResetPosition()
{
	//TODO
}

void FSimulatedHMD::SetBaseRotation(const FRotator&){}

FRotator FSimulatedHMD::GetBaseRotation() const
{
	return FRotator::ZeroRotator;
}

void FSimulatedHMD::SetBaseOrientation(const FQuat&){}

FQuat FSimulatedHMD::GetBaseOrientation() const
{
	return FQuat::Identity;
}

bool FSimulatedHMD::DoesSupportPositionalTracking() const
{
	return true;
}

void FSimulatedHMD::OnBeginRendering_RenderThread(FRHICommandListImmediate& RHICmdList, FSceneViewFamily& ViewFamily)
{
	if (SpectatorScreenController)
	{
		SpectatorScreenController->UpdateSpectatorScreenMode_RenderThread();
	}
}

FIntRect FSimulatedHMD::GetFullFlatEyeRect_RenderThread(FTexture2DRHIRef EyeTexture) const
{
	return FIntRect(0, 0, EyeTexture->GetSizeX(),	EyeTexture->GetSizeY());
}

void FSimulatedHMD::CopyTexture_RenderThread(FRHICommandListImmediate& RHICmdList, FRHITexture2D* SrcTexture, FIntRect SrcRect, FRHITexture2D* DstTexture, FIntRect DstRect, bool bClearBlack, bool bNoAlpha) const
{
	/* Mostly copied from OpenXR HMD */
	
	check(IsInRenderingThread());

	const uint32 ViewportWidth = DstRect.Width();
	const uint32 ViewportHeight = DstRect.Height();
	const FIntPoint TargetSize(ViewportWidth, ViewportHeight);

	const float SrcTextureWidth = SrcTexture->GetSizeX();
	const float SrcTextureHeight = SrcTexture->GetSizeY();
	float U = 0.f, V = 0.f, USize = 1.f, VSize = 1.f;
	if (!SrcRect.IsEmpty())
	{
		U = 0;     // SrcRect.Min.X / SrcTextureWidth;
		V = 0;     // SrcRect.Min.Y / SrcTextureHeight;
		USize = 1; // SrcRect.Width() / SrcTextureWidth;
		VSize = 1; // SrcRect.Height() / SrcTextureHeight;
	}

	FRHITexture* ColorRT = DstTexture->GetTexture2D();
	FRHIRenderPassInfo RenderPassInfo(ColorRT, ERenderTargetActions::DontLoad_Store);
	RHICmdList.BeginRenderPass(RenderPassInfo, TEXT("OpenXRHMD_CopyTexture"));
	{
		if (bClearBlack)
		{
			const FIntRect ClearRect(0, 0, DstTexture->GetSizeX(), DstTexture->GetSizeY());
			RHICmdList.SetViewport(ClearRect.Min.X, ClearRect.Min.Y, 0, ClearRect.Max.X, ClearRect.Max.Y, 1.0f);
			DrawClearQuad(RHICmdList, FLinearColor::Black);
		}

		RHICmdList.SetViewport(DstRect.Min.X, DstRect.Min.Y, 0, DstRect.Max.X, DstRect.Max.Y, 1.0f);

		FGraphicsPipelineStateInitializer GraphicsPSOInit;
		RHICmdList.ApplyCachedRenderTargets(GraphicsPSOInit);
		GraphicsPSOInit.BlendState =
			bNoAlpha
				? TStaticBlendState<>::GetRHI()
				: TStaticBlendState<CW_RGBA, BO_Add, BF_SourceAlpha, BF_InverseSourceAlpha, BO_Add, BF_One, BF_InverseSourceAlpha>::GetRHI();
		GraphicsPSOInit.RasterizerState = TStaticRasterizerState<>::GetRHI();
		GraphicsPSOInit.DepthStencilState = TStaticDepthStencilState<false, CF_Always>::GetRHI();
		GraphicsPSOInit.PrimitiveType = PT_TriangleList;

		const auto FeatureLevel = GMaxRHIFeatureLevel;
		auto ShaderMap = GetGlobalShaderMap(FeatureLevel);

		TShaderMapRef<FScreenVS> VertexShader(ShaderMap);
		TShaderMapRef<FScreenPS> PixelShader(ShaderMap);

		GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GFilterVertexDeclaration.VertexDeclarationRHI;
		GraphicsPSOInit.BoundShaderState.VertexShaderRHI = VertexShader.GetVertexShader();
		GraphicsPSOInit.BoundShaderState.PixelShaderRHI = PixelShader.GetPixelShader();

		SetGraphicsPipelineState(RHICmdList, GraphicsPSOInit);

		RHICmdList.Transition(FRHITransitionInfo(SrcTexture, ERHIAccess::Unknown, ERHIAccess::SRVMask));

		const bool bSameSize = DstRect.Size() == SrcRect.Size();
		if (bSameSize)
		{
			PixelShader->SetParameters(RHICmdList, TStaticSamplerState<SF_Point>::GetRHI(), SrcTexture);
		}
		else
		{
			PixelShader->SetParameters(RHICmdList, TStaticSamplerState<SF_Bilinear>::GetRHI(), SrcTexture);
		}

		RendererModule->DrawRectangle(
			RHICmdList, 0, 0, ViewportWidth, ViewportHeight, U, V, USize, VSize, TargetSize, FIntPoint(1, 1), VertexShader, EDRF_Default);
	}
	RHICmdList.EndRenderPass();
}

FIntPoint FSimulatedHMD::GetIdealRenderTargetSize() const
{
	FIntPoint Size(RecommendedImageRectWidth, RecommendedImageRectHeight);

	// We always prefer the nearest multiple of 4 for our buffer sizes. Make sure we round up here,
	// so we're consistent with the rest of the engine in creating our buffers.
	QuantizeSceneBufferSize(Size, Size);

	return Size;
}

bool FSimulatedHMD::OnStartGameFrame(FWorldContext& WorldContext)
{
	const AWorldSettings* const WorldSettings = WorldContext.World() ? WorldContext.World()->GetWorldSettings() : nullptr;
	if (WorldSettings)
	{
		WorldToMeters = WorldSettings->WorldToMeters;
	}

	// Only refresh this based on the game world.  When remoting there is also an editor world, which we do not want to have affect the
	// transform.
	if (WorldContext.World()->IsGameWorld())
	{
		RefreshTrackingToWorldTransform(WorldContext);
	}

	return true;
}

bool FSimulatedHMD::IsStereoEnabled() const
{
	return true;
}

bool FSimulatedHMD::EnableStereo(bool)
{
	return true;
}

void FSimulatedHMD::AdjustViewRect(EStereoscopicPass StereoPass, int32& X, int32& Y, uint32& SizeX, uint32& SizeY) const
{
	const uint32 ViewIndex = GetViewIndexForPass(StereoPass);

	FIntPoint ViewRectMin(EForceInit::ForceInitToZero);
	for (uint32 i = 0; i < ViewIndex; ++i)
	{
		ViewRectMin.X += RecommendedImageRectWidth;
	}
	QuantizeSceneBufferSize(ViewRectMin, ViewRectMin);

	X = ViewRectMin.X;
	Y = ViewRectMin.Y;
	SizeX = RecommendedImageRectWidth;
	SizeY = RecommendedImageRectHeight;
}

EStereoscopicPass FSimulatedHMD::GetViewPassForIndex(bool bStereoRequested, uint32 ViewIndex) const
{
	if (!bStereoRequested)
		return EStereoscopicPass::eSSP_FULL;

	return static_cast<EStereoscopicPass>(eSSP_LEFT_EYE + ViewIndex);
}

uint32 FSimulatedHMD::GetViewIndexForPass(EStereoscopicPass StereoPassType) const
{
	switch (StereoPassType)
	{
	case eSSP_LEFT_EYE:
	case eSSP_FULL:
		return 0;

	case eSSP_RIGHT_EYE:
		return 1;

	default:
		return StereoPassType - eSSP_LEFT_EYE;
	}
}

int32 FSimulatedHMD::GetDesiredNumberOfViews(bool bStereoRequested) const
{
	return bStereoRequested ? 2 : 1;
}

FMatrix FSimulatedHMD::GetStereoProjectionMatrix(const enum EStereoscopicPass) const
{
	const float ZNear = GNearClippingPlane;
	const float AngleUp = FMath::DegreesToRadians(30.0f);
	const float AngleDown = FMath::DegreesToRadians(-30.0f);
	const float AngleLeft = FMath::DegreesToRadians(-40.0f);
	const float AngleRight = FMath::DegreesToRadians(40.0f);

	const float TanAngleUp = tan(AngleUp);
	const float TanAngleDown = tan(AngleDown);
	const float TanAngleLeft = tan(AngleLeft);
	const float TanAngleRight = tan(AngleRight);

	float SumRL = (TanAngleRight + TanAngleLeft);
	float SumTB = (TanAngleUp + TanAngleDown);
	float InvRL = (1.0f / (TanAngleRight - TanAngleLeft));
	float InvTB = (1.0f / (TanAngleUp - TanAngleDown));

	FMatrix Mat = FMatrix(
		FPlane((2.0f * InvRL), 0.0f, 0.0f, 0.0f), FPlane(0.0f, (2.0f * InvTB), 0.0f, 0.0f),
		FPlane((SumRL * -InvRL), (SumTB * -InvTB), 0.0f, 1.0f), FPlane(0.0f, 0.0f, ZNear, 0.0f));

	return Mat;
}

void FSimulatedHMD::GetEyeRenderParams_RenderThread(const FRenderingCompositePassContext&, FVector2D& EyeToSrcUVScaleValue, FVector2D& EyeToSrcUVOffsetValue) const
{
	EyeToSrcUVOffsetValue = FVector2D::ZeroVector;
	EyeToSrcUVScaleValue = FVector2D(1.0f, 1.0f);
}

IStereoRenderTargetManager* FSimulatedHMD::GetRenderTargetManager()
{
	return this;
}

void FSimulatedHMD::RenderTexture_RenderThread(class FRHICommandListImmediate& RHICmdList, class FRHITexture2D* BackBuffer, class FRHITexture2D* SrcTexture, FVector2D WindowSize) const
{
	if (SpectatorScreenController)
	{
		SpectatorScreenController->RenderSpectatorScreen_RenderThread(RHICmdList, BackBuffer, SrcTexture, WindowSize);
	}
}

bool FSimulatedHMD::ShouldUseSeparateRenderTarget() const
{
	return IsStereoEnabled();
}

void FSimulatedHMD::SetupViewFamily(FSceneViewFamily& InViewFamily)
{
	InViewFamily.EngineShowFlags.MotionBlur = 0;
	InViewFamily.EngineShowFlags.HMDDistortion = true;
	InViewFamily.EngineShowFlags.StereoRendering = false; // IsStereoEnabled();
}

void FSimulatedHMD::SetupView(FSceneViewFamily& InViewFamily, FSceneView& InView)
{
	HMDSimulationManager->GetHeadPose(InView.BaseHmdOrientation, InView.BaseHmdLocation);
}

void FSimulatedHMD::PreRenderView_RenderThread(FRHICommandListImmediate&, FSceneView&){}

void FSimulatedHMD::PreRenderViewFamily_RenderThread(FRHICommandListImmediate&, FSceneViewFamily&){}

bool FSimulatedHMD::IsActiveThisFrame(class FViewport* InViewport) const
{
	return GEngine && GEngine->IsStereoscopic3D(InViewport) && HMDSimulationManager && HMDSimulationManager->IsCurrentlyEnabled();
}

FSimulatedHMD::FSimulatedHMD(const FAutoRegister& AutoRegister)
	: FHeadMountedDisplayBase(nullptr)
	, FSceneViewExtensionBase(AutoRegister)
{

	RendererModule = FModuleManager::GetModulePtr<IRendererModule>("Renderer");

	SpectatorScreenController = MakeUnique<FDefaultSpectatorScreenController>(this);
}

FSimulatedHMD::~FSimulatedHMD(){}

bool FSimulatedHMD::IsInitialized() const
{
	return true;
}

void FSimulatedHMD::SetHMDSimulationController(UHMDSimulationManager* Manager)
{
	HMDSimulationManager = Manager;
}
